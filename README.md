# README

Objetiva agregar idéias de vários contribuidores na definição de Propostas a serem submetidas ao CICN.

Documentos serão escritos em padrão de formatação *Pandoc/Markdown* (exemplos: <http://www.johnmacfarlane.net/pandoc/demos.html>). Esta é uma ferramenta que facilita a conversão para o formato PDF.

Para gerar o PDF use o ferramenta ***Pandoc*** e o comando a seguir:

   - ```pandoc CICN-CRfP-common.md CICN-CRfP-0.md -o CICN-CRfP-0.pdf --toc --template=template/cicn-rfp-latex.template```

Parametrizar o ```git```

   - ```git config --local user.email '<usermail>@exemplo.gov.br'```
   - ```git config --local user.name '<User Name>'```
