---
remark: metadados para a ser usado pelo parser de conversão para pdf
date: 31 de outubro de 2014
tipo_documento: CICN Request for Proposal (CRfP) 
fontsize: 12pt
papersize: a4paper
lang: brazil
mainlang: brazil
type: report
header_row1: Centro de Inovação em Computação em Nuvem
header_row2: (CICN)
...
